const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

// Imports the Google Cloud client library
const {Storage} = require('@google-cloud/storage');

// Your Google Cloud Platform project ID
const projectId = 'proyectotesis-af69a';

// Creates a client
const storage = new Storage({
  projectId: projectId,
});

// The name for the new bucket
const bucketName = 'gs://proyectotesis-af69a.appspot.com';

function moveImg(src, dest){
  var bucket = storage.bucket(bucketName);
  var file = bucket.file(src);
  var newLocation = 'gs://proyectotesis-af69a.appspot.com/' + dest;
  file.move(newLocation, ()=>{
    
  });
}

function copyImg(src, dest){
   var bucket = storage.bucket(bucketName);
   var file = bucket.file(src);
   var newLocation = 'gs://proyectotesis-af69a.appspot.com/' + dest;
   file.copy(newLocation, ()=>{
     
   });  
}

function createF(){
  var bucket = storage.bucket(bucketName);
  
}

exports.moveImage = functions.https.onRequest((req, res) => {
  try {
    moveImg(req.query.src, req.query.dest);
    return res.status(200).send({});
  } catch (error) {
    console.log(error);
  }
})

exports.copyImage = functions.https.onRequest((req, res) => {
  try {
    copyImg(req.query.src, req.query.dest);
    return res.status(200).send({});
  } catch (error) {
    console.log(error);
  }
})

exports.deleteFolder = functions.https.onRequest((req, res) => {
  var bucket = storage.bucket(bucketName);
  bucket.deleteFiles({ prefix: req.query.dir, force:false}).then((response)=>{
    return res.status(200).send({});
  }).catch((error)=>{
    return res.status(400).send({msg:'error'});
  });
  
})





